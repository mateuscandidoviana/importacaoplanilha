# Importador de Planilhas

## Sobre o projeto

Permite importar planilhas no formato <em>.xls e xlsx</em> contendo as seguintes colunas: <em>Data Entrega, Nome do Produto, Quantidade e Valor Unitário.</em>

## Feito com

### Backend
- [NET Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) - .NET Core 3.1 
- [EntityFrameworkCore](https://docs.microsoft.com/pt-br/ef/core/) - EntityFramework Core 3.1
- [Fluentvalidation](https://fluentvalidation.net/) - Fluent Validation
- [AutoMapper](https://automapper.org/) - AutoMapper
- [ExcelDataReader](https://github.com/ExcelDataReader/ExcelDataReader) - ExcelDataReader
- [Swagger](https://swagger.io/) - Swagger


### Frontend
- [Angular](https://angular.io//) - Angular
- [Material](https://material.angular.io/components/icon/overview) - Material