﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ImportacaoPlanilha.Api.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Importacoes",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DataImportacao = table.Column<DateTime>(nullable: false),
                    NomeArquivo = table.Column<string>(nullable: false, maxLength: 100)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Importacoes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Importacoes");
        }
    }
}
