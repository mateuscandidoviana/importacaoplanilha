﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ImportacaoPlanilha.Api.Migrations
{
    public partial class Falhas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Falhas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(nullable: false, maxLength: 255),
                    Linha = table.Column<long>(nullable: false),
                    ImportacaoId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Falhas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Falhas_Importacoes_ImportacaoId",
                        column: x => x.ImportacaoId,
                        principalTable: "Importacoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Falhas_ImportacaoId",
                table: "Falhas",
                column: "ImportacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Falhas");
        }
    }
}
