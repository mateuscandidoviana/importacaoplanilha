﻿using ImportacaoPlanilha.Api.Models.Base;
using ImportacaoPlanilha.Api.Models.Enumeradores;
using ImportacaoPlanilha.Api.Models.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ImportacaoPlanilha.Api.Models
{
    public class Importacao : Entity
    {
        public Importacao(string nomeArquivo)
        {
            NomeArquivo = nomeArquivo;
            DataImportacao = DateTime.Now;
            Situacao = SituacaoImportacaoEnum.Importado;
            _falhas = new List<Falha>();
            _lotes = new List<Lote>();
            Validar();
        }

        protected Importacao() { }

        public DateTime DataImportacao { get; private set; }
        public string NomeArquivo { get; private set; }
        public SituacaoImportacaoEnum Situacao { get; private set; }

        private readonly List<Falha> _falhas;
        public IReadOnlyCollection<Falha> Falhas => _falhas;

        private readonly List<Lote> _lotes;
        public IReadOnlyCollection<Lote> Lotes => _lotes;

        public void AddFalha(Falha falha) => _falhas.Add(falha);
        public void AddLote(Lote lote)
        {
            if (lote.Invalid) return;

            _lotes.Add(lote);
        }

        public void AtualizarSituacao()
        {
            if (_falhas.Any())
                Situacao = SituacaoImportacaoEnum.Falha;            
            else
                Situacao = SituacaoImportacaoEnum.Sucesso;
        }

        public override bool Validar()
        {
            return Validate(this, new ImportacaoValidator());
        }
    }
}
