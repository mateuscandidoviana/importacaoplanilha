﻿using ImportacaoPlanilha.Api.Models.Base;
using ImportacaoPlanilha.Api.Models.Validators;

namespace ImportacaoPlanilha.Api.Models
{
    public class Falha : Entity
    {
        public Falha(string descricao, long linha, Importacao importacao)
        {
            Descricao = descricao;
            Linha = linha;
            Importacao = importacao;
            Validar();
        }

        protected Falha() { }

        public string Descricao { get; private set; }
        public long Linha { get; private set; }
        public Importacao Importacao { get; private set; }


        public override bool Validar()
        {
            return Validate(this, new FalhaValidator());
        }
    }
}
