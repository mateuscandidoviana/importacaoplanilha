﻿namespace ImportacaoPlanilha.Api.Models.Enumeradores
{
    public enum SituacaoImportacaoEnum
    {
        Importado = 0,
        Sucesso = 1,
        Falha = 2
    }
}
