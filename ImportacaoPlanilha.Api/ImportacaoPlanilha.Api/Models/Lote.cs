﻿using ImportacaoPlanilha.Api.Models.Base;
using ImportacaoPlanilha.Api.Models.Validators;
using System;

namespace ImportacaoPlanilha.Api.Models
{
    public class Lote : Entity
    {
        public Lote(DateTime dataEntrega, string nomeProduto, int quantidade, decimal valorUnitario, Importacao importacao)
        {
            DataEntrega = dataEntrega;
            NomeProduto = nomeProduto;
            Quantidade = quantidade;
            SetValorUnitario(valorUnitario);
            Importacao = importacao;
            Validar();
        }

        protected Lote() { }

        public DateTime DataEntrega { get; private set; }
        public string NomeProduto { get; private set; }
        public int Quantidade { get; private set; }
        public decimal ValorUnitario { get; private set; }
        public Importacao Importacao { get; private set; }

        public void SetValorUnitario(decimal valorUnitario) => ValorUnitario = decimal.Round(valorUnitario, 2);

        public override bool Validar()
        {
            return Validate(this, new LoteValidator());
        }
    }
}
