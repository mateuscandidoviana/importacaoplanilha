﻿using ImportacaoPlanilha.Api.Models.Base;

namespace ImportacaoPlanilha.Api.Models.Validators
{
    public interface IEntityValidator
    {
        void Validate(params Entity[] entities);
    }
}
