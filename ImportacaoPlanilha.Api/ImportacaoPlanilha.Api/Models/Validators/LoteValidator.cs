﻿using FluentValidation;
using System;

namespace ImportacaoPlanilha.Api.Models.Validators
{
    public class LoteValidator : AbstractValidator<Lote>
    {
        public LoteValidator()
        {
            RuleFor(x => x.DataEntrega).Must(DataValida).WithMessage("Data de entrega não pode ser menor ou igual que o dia atual");
            RuleFor(x => x.NomeProduto).NotEmpty().WithMessage("Nome do produto deve ser valido")
                                       .MaximumLength(50).WithMessage("Nome do produto precisa ter o tamanho máximo de 50 caracteres");
            RuleFor(x => x.Quantidade).NotNull().WithMessage("Quantidade deve ser valida")
                                      .GreaterThan(0).WithMessage("Quantidade tem que ser maior do que zero");
            RuleFor(x => x.ValorUnitario).NotNull().WithMessage("Valor unitário deve ser valido")
                                      .GreaterThan(0).WithMessage("Valor unitário tem que ser maior do que zero");
            RuleFor(x => x.Importacao).NotNull().WithMessage("Importação deve ser valida");
        }

        private static bool DataValida(DateTime data) => data.Date >= DateTime.Now.Date;
    }    
}