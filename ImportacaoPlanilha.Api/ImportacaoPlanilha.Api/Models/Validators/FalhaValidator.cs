﻿using FluentValidation;

namespace ImportacaoPlanilha.Api.Models.Validators
{
    public class FalhaValidator : AbstractValidator<Falha>
    {
        public FalhaValidator()
        {
            RuleFor(x => x.Descricao).NotEmpty().WithMessage("Descrição da falha deve ser preenchida")
                                     .MaximumLength(255).WithMessage("Descrição da falha deve conter 255 caracteres");

            RuleFor(x => x.Linha).GreaterThan(0).WithMessage("Linha invalida");

            RuleFor(x => x.Importacao).NotNull().WithMessage("Importação deve ser valida");
        }
    }
}
