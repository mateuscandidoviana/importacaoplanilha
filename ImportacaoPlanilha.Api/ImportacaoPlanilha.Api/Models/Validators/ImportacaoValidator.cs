﻿using FluentValidation;

namespace ImportacaoPlanilha.Api.Models.Validators
{
    public class ImportacaoValidator : AbstractValidator<Importacao>
    {
        public ImportacaoValidator()
        {
            RuleFor(x => x.NomeArquivo).NotEmpty().WithMessage("Nome do Arquivo deve ser preenchido")
                                       .MaximumLength(100).WithMessage("Nome do Arquivo precisa ter o tamanho máximo de 100 caracteres");
        }
    }
}
