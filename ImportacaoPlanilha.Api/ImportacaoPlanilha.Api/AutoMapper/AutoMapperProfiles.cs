﻿using AutoMapper;
using ImportacaoPlanilha.Api.Application.Dto.Response;
using ImportacaoPlanilha.Api.Models;
using System.Linq;

namespace ImportacaoPlanilha.Api.AutoMapper
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Importacao, ImportacaoResponse>();
            CreateMap<Importacao, ListaImportacaoResponse>()
                .ForMember(x => x.NumeroItens, y => y.MapFrom(z => z.Lotes.Count()))
                .ForMember(x => x.MenorDataEntrega, y => y.MapFrom(z => z.Lotes.OrderBy(d => d.DataEntrega).FirstOrDefault().DataEntrega))
                .ForMember(x => x.ValorTotalImportacao, y => y.MapFrom(z => z.Lotes.Sum(a => a.ValorUnitario * a.Quantidade)));

            CreateMap<Lote, LoteResponse>();

            CreateMap<Falha, FalhaResponse>();

        }
    }
}
