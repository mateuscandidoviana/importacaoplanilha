﻿using ImportacaoPlanilha.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportacaoPlanilha.Api.Data.Mappings
{
    public class ImportacaoMapping : IEntityTypeConfiguration<Importacao>
    {
        public void Configure(EntityTypeBuilder<Importacao> builder)
        {
            builder.ToTable("Importacoes");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.NomeArquivo)
                   .IsRequired()
                   .HasColumnType("varchar(100)");

            builder.Property(x => x.DataImportacao)
                   .IsRequired()
                   .HasColumnType("datetime2");

            builder.Property(x => x.Situacao)
                   .IsRequired()
                   .HasColumnType("varchar(30)");
        }
    }
}
