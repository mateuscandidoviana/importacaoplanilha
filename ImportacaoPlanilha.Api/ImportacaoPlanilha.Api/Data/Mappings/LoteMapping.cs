﻿using ImportacaoPlanilha.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportacaoPlanilha.Api.Data.Mappings
{
    public class LoteMapping : IEntityTypeConfiguration<Lote>
    {
        public void Configure(EntityTypeBuilder<Lote> builder)
        {
            builder.ToTable("Lotes");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.DataEntrega)
                   .IsRequired()
                   .HasColumnType("datetime2");

            builder.Property(x => x.NomeProduto)
                   .IsRequired()
                   .HasColumnType("varchar(50)");

            builder.Property(x => x.Quantidade)
                   .IsRequired()
                   .HasColumnType("int");

            builder.Property(x => x.ValorUnitario)
                   .IsRequired()
                   .HasColumnType("decimal(18,2)");

            builder.HasOne(l => l.Importacao)
                   .WithMany(f => f.Lotes);
        }
    }
}
