﻿using ImportacaoPlanilha.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportacaoPlanilha.Api.Data.Mappings
{
    public class FalhaMapping : IEntityTypeConfiguration<Falha>
    {
        public void Configure(EntityTypeBuilder<Falha> builder)
        {
            builder.ToTable("Falhas");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Descricao)
                   .IsRequired()
                   .HasColumnType("varchar(255)");

            builder.Property(x => x.Linha)
                   .IsRequired()
                   .HasColumnType("bigint");

            builder.HasOne(l => l.Importacao)
                   .WithMany(f => f.Falhas);
        }
    }
}
