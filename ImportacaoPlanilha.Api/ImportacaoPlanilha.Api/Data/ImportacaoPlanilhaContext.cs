﻿using ImportacaoPlanilha.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace ImportacaoPlanilha.Api.Data
{
    public class ImportacaoPlanilhaContext : DbContext
    {
        public ImportacaoPlanilhaContext(DbContextOptions<ImportacaoPlanilhaContext> options) : base(options) { }

        public DbSet<Importacao> Importacoes { get; set; }
        public DbSet<Falha> Falhas { get; set; }
        public DbSet<Lote> Lotes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ImportacaoPlanilha;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}
