﻿using ImportacaoPlanilha.Api.Application.Dto.Response;
using ImportacaoPlanilha.Api.Application.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImportacaoPlanilha.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportacaoController : ControllerBase
    {
        private readonly IImportacaoService _importacaoService;

        public ImportacaoController(IImportacaoService importacaoService) => _importacaoService = importacaoService;

        /// <summary>
        /// Obtem todas as importações
        /// </summary>
        /// <returns>Importações</returns>
        [HttpGet]
        [Produces(typeof(List<ListaImportacaoResponse>))]
        public async Task<IActionResult> Get()
        {
            return Ok(await _importacaoService.Obter());
        }

        /// <summary>
        /// Obtem importação por Id
        /// </summary>
        /// <param name="id">Identificador da Importação</param>
        /// <returns>Importação</returns>
        [HttpGet("{id}")]
        [Produces(typeof(ImportacaoResponse))]
        public async Task<IActionResult> GetById(long id)
        {
            var importacao = await _importacaoService.Obter(id);

            if(importacao == null)
            {
                return NotFound();
            }

            return Ok(importacao);
        }

        /// <summary>
        /// Insere uma nova importação por arquivo
        /// </summary>
        /// <param name="file">Planilha</param>
        /// <returns>Importação</returns>
        [HttpPost]
        [Produces(typeof(ImportacaoResponse))]
        public async Task<IActionResult> Post([FromForm] IFormFile file)
        {
            var response = await _importacaoService.Importar(file);

            if(response == null)
            {
                return BadRequest();
            }

            return Ok(response);
        }
    }
}
