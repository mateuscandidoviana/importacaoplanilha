﻿using ImportacaoPlanilha.Api.Application.Dto.Response;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ImportacaoPlanilha.Api.Application.Services.Interfaces
{
    public interface IImportacaoService
    {
        Task<ImportacaoResponse> Importar(IFormFile file);
        Task<List<ListaImportacaoResponse>> Obter();
        Task<ImportacaoResponse> Obter(long id);
    }
}
