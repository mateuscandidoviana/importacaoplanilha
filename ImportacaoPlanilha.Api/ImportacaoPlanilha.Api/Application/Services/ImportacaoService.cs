﻿using AutoMapper;
using ImportacaoPlanilha.Api.Application.Dto.Response;
using ImportacaoPlanilha.Api.Application.Notify;
using ImportacaoPlanilha.Api.Application.Services.Interfaces;
using ImportacaoPlanilha.Api.Data;
using ImportacaoPlanilha.Api.Helper.Interface;
using ImportacaoPlanilha.Api.Models;
using ImportacaoPlanilha.Api.Models.Base;
using ImportacaoPlanilha.Api.Models.Validators;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ImportacaoPlanilha.Api.Application.Services
{
    public class ImportacaoService : IImportacaoService
    {
        private readonly ImportacaoPlanilhaContext _context;
        private readonly NotificationContext _notification;
        private readonly IEntityValidator _entityValidator;
        private readonly IExcelReader _excelReader;
        private readonly IMapper _mapper;

        public ImportacaoService(ImportacaoPlanilhaContext context, NotificationContext notification, IEntityValidator entityValidator, IExcelReader excelReader, IMapper mapper)
        {
            _context = context;
            _notification = notification;
            _entityValidator = entityValidator;
            _excelReader = excelReader;
            _mapper = mapper;
        }

        public async Task<ImportacaoResponse> Importar(IFormFile file)
        {
            var dataTable = _excelReader.Converter(file);

            if (dataTable == null)
            {
                _notification.AddNotification("Aquivo", "Arquivo invalido");
                return null;
            }

            var importacao = new Importacao(file.FileName);

            var linha = 1;

            foreach (DataRow row in dataTable.Rows)
            {
                linha++;

                var lote = new Lote(row.Field<DateTime>(0), row.Field<string>(1), (int)row.Field<double>(2), (decimal)row.Field<double>(3), importacao);

                if (lote.Valid)
                {
                    importacao.AddLote(lote);
                }
                else
                {
                    var falhas = lote.ValidationResult.Errors.Select(x => x.ErrorMessage);

                    foreach (var falha in falhas)
                    {
                        importacao.AddFalha(new Falha(falha, linha, importacao));
                    }
                }                
            }

            importacao.AtualizarSituacao();

            _entityValidator.Validate(new Entity[] { importacao });

            if (_notification.HasNotifications)
            {
                return null;
            }

            _context.Importacoes.Add(importacao);
            await _context.SaveChangesAsync();

            return _mapper.Map<ImportacaoResponse>(importacao);
        }

        public async Task<List<ListaImportacaoResponse>> Obter()
        {
            var importacoes = await _context.Importacoes.Include(x => x.Lotes)
                                                        .Include(x => x.Falhas)
                                                        .ToListAsync();

            return _mapper.Map<List<Importacao>, List<ListaImportacaoResponse>>(importacoes);
        }

        public async Task<ImportacaoResponse> Obter(long id)
        {
            var importacao = await _context.Importacoes.Include(x => x.Lotes)
                                                       .Include(x => x.Falhas)
                                                       .FirstOrDefaultAsync(x => x.Id == id);
            
            return _mapper.Map<ImportacaoResponse>(importacao);
        }
    }
}
