﻿using System;

namespace ImportacaoPlanilha.Api.Application.Dto.Response
{
    public class ListaImportacaoResponse
    {
        public long Id { get; set; }
        public DateTime DataImportacao { get; set; }
        public long NumeroItens { get; set; }
        public DateTime? MenorDataEntrega { get; set; }
        public decimal ValorTotalImportacao { get; set; }
        public string Situacao { get; set; }
    }
}
