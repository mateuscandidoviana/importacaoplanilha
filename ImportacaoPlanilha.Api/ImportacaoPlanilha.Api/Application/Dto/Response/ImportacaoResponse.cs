﻿using System;
using System.Collections.Generic;

namespace ImportacaoPlanilha.Api.Application.Dto.Response
{
    public class ImportacaoResponse
    {
        public long Id { get; set; }
        public DateTime DataImportacao { get; set; }
        public string NomeArquivo { get; set; }
        public string Situacao { get; set; }
        public List<FalhaResponse> Falhas { get; set; }
        public List<LoteResponse> Lotes { get; set; }
    }
}
