﻿using System;

namespace ImportacaoPlanilha.Api.Application.Dto.Response
{
    public class LoteResponse
    {
        public long Id { get; set; }
        public DateTime DataEntrega { get; set; }
        public string NomeProduto { get; set; }
        public int Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
        public long ImportacaoId { get; set; }
    }
}
