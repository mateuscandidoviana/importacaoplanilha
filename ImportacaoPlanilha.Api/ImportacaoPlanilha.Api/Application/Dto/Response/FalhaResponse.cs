﻿namespace ImportacaoPlanilha.Api.Application.Dto.Response
{
    public class FalhaResponse
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public long Linha { get; set; }
        public long ImportacaoId { get; set; }
    }
}
