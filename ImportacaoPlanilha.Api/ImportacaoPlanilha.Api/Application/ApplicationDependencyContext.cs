﻿using ImportacaoPlanilha.Api.Application.Notify;
using ImportacaoPlanilha.Api.Application.Services;
using ImportacaoPlanilha.Api.Application.Services.Interfaces;
using ImportacaoPlanilha.Api.Data;
using ImportacaoPlanilha.Api.Helper;
using ImportacaoPlanilha.Api.Helper.Interface;
using ImportacaoPlanilha.Api.Models.Validators;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ImportacaoPlanilha.Api.Application
{
    public static class ApplicationDependencyContext
    {
        public static void ConfigureApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ImportacaoPlanilhaContext>();
            services.AddScoped<NotificationContext>();
            services.AddScoped<IEntityValidator, EntityValidator>();
            services.AddScoped<IImportacaoService, ImportacaoService>();
            services.AddScoped<IExcelReader, ExcelReader>();
        }
    }
}
