﻿using ExcelDataReader;
using ImportacaoPlanilha.Api.Helper.Interface;
using Microsoft.AspNetCore.Http;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace ImportacaoPlanilha.Api.Helper
{
    public class ExcelReader : IExcelReader
    {
        private string[] allowedExtensions = new[] { ".xls", ".xlsx" };
        private string[] expectedColumns = new[] { "Data Entrega", "Nome do Produto", "Quantidade", "Valor Unitário" };

        public DataTable Converter(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return null;

            var extension = Path.GetExtension(file.FileName);

            if (!allowedExtensions.Contains(extension))
                return null;

            DataTable result;

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            using (var stream = new MemoryStream())
            {
                file.CopyTo(stream);
                stream.Position = 0;
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var config = new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = _ => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    };

                    result = reader.AsDataSet(config).Tables[0];
                }
            }

            foreach (DataColumn column in result.Columns)
            {
                if (!expectedColumns.Contains(column.ColumnName))
                {
                    return null;
                }
            }

            return result;
        }
    }
}
