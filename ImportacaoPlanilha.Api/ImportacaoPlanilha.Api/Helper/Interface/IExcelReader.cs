﻿using Microsoft.AspNetCore.Http;
using System.Data;

namespace ImportacaoPlanilha.Api.Helper.Interface
{
    public interface IExcelReader
    {
        DataTable Converter(IFormFile file);
    }
}
