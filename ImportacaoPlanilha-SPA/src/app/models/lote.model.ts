export class LoteModel {
    id: string;
    dataEntrega: Date;
    nomeProduto: string;
    quantidade: number;
    valorUnitario: number
}