import { FalhaModel } from './falha.model';
import { LoteModel } from './lote.model';

export class ImportacaoModel {
    id: string;
    dataImportacao: Date;
    nomeArquivo: string;
    situacao: string;
    lotes: LoteModel[];
    falhas: FalhaModel[]
}