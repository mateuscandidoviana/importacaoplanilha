export class ListaImportacaoModel {
    id: string;
    dataImportacao: Date;
    numeroItens: number;
    menorDataEntrega: Date;
    valorTotalImportacao: number;
}