import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { ImportacaoService } from '../../services/importacao.service';

@Component({
    selector: 'app-envio',
    templateUrl: './envio.component.html',
    styleUrls: ['./envio.component.css']
})
export class EnvioComponent implements OnInit {
    file: File;
    constructor(
        private service: ImportacaoService,
        private router: Router,
        private alertService: AlertService) { }

    ngOnInit(): void {
    }

    setFile(files: File[]) {
        if (files.length > 0) {
            this.file = files[0];
        }
    }

    sendFile() {
        this.service.insert(this.file).subscribe(sub => {
            this.alertService.notify("Arquivo enviado com sucesso!");
            this.router.navigate(["/"]);
        },
        err =>{
            this.alertService.notify(err.error[0].message);
        });
    }

}
