import { Component, OnInit } from '@angular/core';
import { ListaImportacaoModel } from '../../models/lista-importacao.model';
import { ImportacaoService } from '../../services/importacao.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    importacoes: ListaImportacaoModel[];
    displayedColumns = ['id', 'dataImportacao', 'numeroItens', 'menorDataEntrega', 'valorTotalImportacao', 'situacao']

    constructor(
        private service: ImportacaoService
    ) { }

    ngOnInit(): void {
        this.service.get().subscribe(sub =>{
            this.importacoes = sub;
        })
    }

}
