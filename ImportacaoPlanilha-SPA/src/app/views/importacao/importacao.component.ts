import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FalhaModel } from '../../models/falha.model';
import { ImportacaoModel } from '../../models/importacao.model';
import { LoteModel } from '../../models/lote.model';
import { ImportacaoService } from '../../services/importacao.service';

@Component({
    selector: 'app-importacao',
    templateUrl: './importacao.component.html',
    styleUrls: ['./importacao.component.css']
})
export class ImportacaoComponent implements OnInit {
    id: number;
    importacao: ImportacaoModel;
    lotes: LoteModel[];
    falhas: FalhaModel[];
    displayedLoteColumns = ['dataEntrega', 'nomeProduto', 'quantidade', 'valorUnitario']
    displayedFalhaColumns = ['linha', 'descricao']

    constructor(
        private service: ImportacaoService,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit(): void {
        this.route.params.subscribe(data => {
            this.service.getById(data["id"]).subscribe(sub => {
                this.id = data["id"];
                this.importacao = sub;
                this.lotes = sub.lotes;
                this.falhas = sub.falhas;
            })
        });
    }
}
