import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImportacaoModel } from '../models/importacao.model';
import { ListaImportacaoModel } from '../models/lista-importacao.model';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class ImportacaoService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    insert(file: File) {
        const formData: FormData = new FormData();
        formData.append('file', file, file.name);
        return this.http.post(this.baseUrl + 'importacao', formData);
    }

    get(): Observable<ListaImportacaoModel[]> {
        return this.http.get<ListaImportacaoModel[]>(this.baseUrl + 'importacao')
    }

    getById(id: number): Observable<ImportacaoModel> {
        return this.http.get<ImportacaoModel>(this.baseUrl + `importacao/${id}`)
    }
}