import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class AlertService {
    constructor(private snackBar: MatSnackBar) { }

    notify(text: string) {
        this.snackBar.open(text, 'Fechar', {
            duration: 1500,
            horizontalPosition: 'right',
            verticalPosition: 'top',
        });
    }
}