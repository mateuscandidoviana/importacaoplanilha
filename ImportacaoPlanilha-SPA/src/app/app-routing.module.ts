import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnvioComponent } from './views/envio/envio.component';
import { HomeComponent } from './views/home/home.component';
import { ImportacaoComponent } from './views/importacao/importacao.component';


const routes: Routes = [
    {
        path: "",
        component: HomeComponent
    },
    {
        path: "importacao/:id",
        component: ImportacaoComponent
    },
    {
        path: "envio",
        component: EnvioComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
